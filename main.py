"""Execution file.
Extraction is by default *disabled*. Remove hash to execute extraction.
This script takes H2S and H2O data in .csv format in the same directory
and creates two energy surface plots for each molecule.
"""
import numpy as np
import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#import os # required to execute extractor

from source import C2v_3Atoms, extractor

# Extract the data into a single .csv file
# Remove the hash to test the extractor
#H2O_extractor = extractor()    # extractor object from extractor.py for H2O
#H2O_extractor.extract_from_dir('H2Ooutfiles')
#H2O_extractor.extract_to_csv('H2O.csv')

#H2S_extractor = extractor()    # another instance of extractor for H2S
#H2S_extractor.extract_from_dir('H2Soutfiles')
#H2S_extractor.extract_to_csv('H2S.csv')

# Produce the two plots(over all data and near minimum) for each compound
H2O = C2v_3Atoms('H2O', 'H2O.csv')
H2O.get_data_from_csv()
H2O.plot()

H2S = C2v_3Atoms('H2S', 'H2S.csv')
H2S.get_data_from_csv()
H2S.plot()

# Obtain the vibrational frequencies
H2O.vib_frequency()
print("=======================================================================")
H2S.vib_frequency()
