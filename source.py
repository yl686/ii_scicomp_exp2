import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import os
import csv
import scipy.constants as const

class extractor:
    """Reader and extractor objects to read the raw result files, extract (r,
    theta, energy) and concentrate into a .csv file.
    Note that one only needs to extract the data once for the project.

    Variables:
        data    list    [r, theta, energy] from each source file as an item
    Methods:
        get_data(file_item)
            extract the relevant figures from the data files of given format
        extract_from_dir(self, dir_name)
            extract [r, theta, energy] from all the outfiles in [dir_name]
        extract_to_csv(self, csv_file_name)
            creeate .csv file with head "r,theta,energy" and entries
            [r,theta,energy] in self.data
    """

    @staticmethod
    def get_data(file_item):
        r_theta_e = []
        with open(file_item,"r") as f:
            count = 0
            lines = f.readlines()
            for line in lines:
                if "Input orientation:" in line:
                    words = lines[count-2].split()
                    r_theta_e.append(words[2])
                    r_theta_e.append(words[4])
                elif "SCF Done: " in line:
                    energy = line.split()
                    r_theta_e.append(energy[4])
                count += 1
        return r_theta_e

    def extract_from_dir(self, dir_name):
        """extract [r,theta,energy] from all the files in a specified directory.
        the program assumes the directory exists as a subdirectory of the directory this script is in.
        INPUT = string containing the name of the directory
        OUTPUT = (i) prints the number of data sets
             (ii)returns list of the data sets: ie [[r,theta,E] , [r,theta,E] ... ]
    """

        directory = "./{}/".format(dir_name)
        H2O_data = []
        fcount = 0
        for f in os.scandir(directory):
            entry = extractor.get_data(directory+f.name)
            H2O_data.append(entry)
            fcount += 1

        self.data = H2O_data
        return

    def extract_to_csv(self, csv_file_name):
        """Create .csv file using the data stored in self.data"""
        # Receive lines to write as a list containing lists of values
        try:
            lines_to_write = self.data
        except AttributeError:
            print("Need data input before extraction.")
            dir_name = input("Name of the directory containing input data:")
            self.extract_from_dir(dir_name)
            self.extract_to_csv(csv_file_name)

        with open(csv_file_name, 'w') as f:
            write = csv.writer(f)
            write.writerow(['r', 'theta', 'energy'])
            write.writerows(lines_to_write)
        return


class C2v_3Atoms:
    """ An instance of this class stores data for a specific molecule, and has
    methods to process data in a file and produce energy surface plots.
    Every series of (r,theta,energy) data for a particular compound may be
    handled by an instance of this class for itself.

    Variables:
        name       str   name of the molecule or ion
        file       str   name of the file containing extraction of
                         [r, theta, energy]
        r          list  r values in order of unpacking
        theta      list  theta values in order of unpacking
        energy     list  energy values in order of unpacking

    Methods:
        get_data_from_csv(self)
            unpacks csv file of [r, theta, energy] with headings and
            appends the values to instance variables
        get_data_from_txt(self)
            unpacks .txt or .dat files. Same functionality as above
        plot(self)
            generates 3D meshplots of energy against (r,theta).
            One is over the entire data range, and the other is near
            (0.002%) around the minimum energy.
        vib_frequency(self)
            calculates vibrational frequencies of symmetric stretch and
            bending modes in wavenumbers. Presents results on terminal.
            Output: (v1(str), v2(str)) v1=sym stretch, v2=bending
    """
    def __init__(self, molecule_name, data_file):
        """ Two required arguments:
        molecule_name = the name of the molecule, for the purpose here "H2O"
            or "H2S"
        data_file = single data file (csv or txt) containing [r, theta, energy]
           values.
        """
        self.name = molecule_name
        self.file = data_file
        self.r = []
        self.theta = []
        self.energy = []

    def get_data_from_csv(self):
        """Read in data values from the file stored at instantiation
        and process into instance attributes.
        """
        with open(self.file,'r') as f:
            read = csv.DictReader(f)
            for row in read:
                self.r.append(float(row['r']))
                self.theta.append(float(row['theta']))
                self.energy.append(float(row['energy']))

    def get_data_from_txt(self):
        # This method is not used in main.py script.
        # However, this was added so that common filetypes are handled.
        r, theta, energy = np.loadtxt(self.file,delimiter=", ",
                                      skiprows=1,unpack=True)
        self.r += r
        self.theta += theta
        self.energy += energy

    def plot(self):
        """A largely self-contained function for producing surfaceplot
        from the data stored as attributes.

        'near-minimum' is defined by the energy difference less than
        0.2 % of the magnitude of the minimum energy.

        Plot 1- energy surface plot over the entire data range provided
        Plot 2- surface plot near the minimum
        """
        def reduce_to_list(ls):    # Remove repeated entries to a new list
            new_list = []
            for item in ls:
                if item not in new_list:
                    new_list.append(item)
            new_list.sort()
            return new_list

        def get_z(x1,y1):    # for x,y pair get cossesponding z value
            count = 0
            for x,y in zip(self.r,self.theta):
                if x1 == x and y1 == y:
                    z = self.energy[count]
                    break
                count += 1
            return z

        x = reduce_to_list(self.r)
        y = reduce_to_list(self.theta)
        X,Y = np.meshgrid(x,y)
        z = [get_z(x1,y1) for x1,y1 in zip(np.ravel(X),np.ravel(Y))]
        Z = np.asarray(z)
        Z = Z.reshape(np.shape(X))

        # Determine (r, theta) for the minimum energy
        where_is_min = np.where(Z==min(z))
        near_min = np.where(Z <= min(z)*0.998) # note that min(z) is negative
        # x, y, z have the same shape, so the selected row, col for z
        # can be directly used as indices for x, y
        row_min, row_max = min(near_min[0]), max(near_min[0])
        col_min, col_max = min(near_min[1]), max(near_min[1])
        trunc_X = X[row_min:row_max,col_min:col_max]
        trunc_Y = Y[row_min:row_max,col_min:col_max]
        trunc_Z = Z[row_min:row_max,col_min:col_max]

        # First plot - over all data range
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.elev = 40    # this angle was manually set to give nice view.
        ax.plot_surface(X,Y,Z,cmap=cm.coolwarm)
        ax.set_xlabel("r / Angstroms")
        ax.set_ylabel("Theta / degrees")
        ax.set_zlabel("Energy / Hartrees")
        plt.title("The Potential Energy Surface for {}".format(self.name))

        plt.savefig("{}_all.png".format(self.name), dpi=600)

        # Second plot - near minimum
        fig2 = plt.figure()
        ax2 = fig2.gca(projection='3d')
        ax2.plot_surface(trunc_X, trunc_Y, trunc_Z, cmap=cm.coolwarm)
        ax2.set_xlabel("r / Angstroms")
        ax2.set_ylabel("Theta / degrees")
        ax2.set_zlabel("Energy / Hartrees")
        plt.title("The Potential Energy Surface for {} Near Energy Minimum".format(self.name))

        plt.savefig("{}_min.png".format(self.name), dpi=600)


    def vib_frequency(self):
        """Determines vibration frequencies of the symmetric stretch
        and bending modes.
        Prints the results on terminal and returns (v_stretch, v_bend)
        """

        print("\nCalculating the frequencies of symmetric stretch and bending modes of {}...\n".format(self.name))

        energy = np.asarray(self.energy)
        theta = np.asarray(self.theta)
        r = np.asarray(self.r)

        min_energy = min(energy)
        min_index = np.where(energy==min_energy)[0][0]
        near_index = np.where(energy<=min_energy*0.999)[0]
        min_theta = theta[min_index] # theta and r are ordered
        min_r = r[min_index]

        def find_energy(r_val, theta_val,dir=1):
            """Finds the shared index for given r, theta near
            equilibrium and returns the corresponding r, theta, energy
            values.
            dir states which direction from equilibrium:
            set to -1 if r < r_eq.
            """
            # sets the endpoint of the range
            if dir == -1:
                end_index = near_index[0]
            else:
                end_index = near_index[-1]

            index = 0
            for n in range(min_index,end_index,dir): # dir controls iter step
                if np.allclose(r[n],r_val) and np.allclose(theta[n],theta_val):
                    index = n
                    break
            return energy[index]

        def calc_k(x1,e1,x2,e2):
            """Calculates either k_r or k_theta by varying one and
            fixing the other. This is a simple simultaneous eqn.
            x1 an x2 are the distance from equilibrium, and e1 and e2
            are energies either both absolute or relative to equilibrium.
            """
            k_x = 2*(e2-e1) / (x2**2 - x1**2)
            return k_x

        # determine kr
        en_1 = find_energy(min_r+0.05,min_theta)
        en_2 = find_energy(min_r+0.1,min_theta)
        k_r = calc_k(0.05,en_1,0.1,en_2)
        # determine k_theta
        en_1 = find_energy(min_r,min_theta+1)
        en_2 = find_energy(min_r,min_theta+2)
        k_theta = calc_k(1,en_1,2,en_2)

        # IMPORTANT: units of k are hartree ang^-2 and hartree deg^-2 each
        # Separates k values and convert units into J m^-2 and J (rad^-2)
        unit_conversion = const.physical_constants["atomic unit of energy"][0]
        k_r = k_r * unit_conversion * 1e20 # J/a.u. m^-2/Ang^-2
        k_theta = k_theta * unit_conversion * (2*np.pi/360)**(-2) # J/a.u.
                                                               # rad^-2/deg^-2
        # calculate reduced masses mu1 and mu2
        reduced_m_1  = 2 * const.u
        reduced_m_2 = 0.5 * const.u
        reduced_m_1 = 2 * const.u
        reduced_m_2 = 0.5 * const.u

        # calculate the vib frequencies
        r_sq_m = (min_r*1e-10) ** 2 # r_eq squared in m^2
        v_stretch = np.sqrt(k_r/reduced_m_1) / (2*const.pi*const.c*100)
        v_bend = np.sqrt(k_theta/(reduced_m_2*r_sq_m)) / (2*const.pi*const.c*100)

        # present results
        print("Frequencies in cm^-1\n")
        # Presents results to 2dp, although it is not a valid precision
        # if the figures are compared with experimental values.
        print("Symmetric stretch mode: {:.2f}".format(v_stretch))
        print("Bending mode: {:.2f}\n".format(v_bend))

        return v_stretch, v_bend
